package com.rojanommar.rest.api.rindus.service;

import com.rojanommar.rest.api.rindus.dao.UserRepository;
import com.rojanommar.rest.api.rindus.model.Iban;
import com.rojanommar.rest.api.rindus.model.User;
import com.rojanommar.rest.api.rindus.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private static final String FIRST_NAME = "FirstName";
    private static final String NEW_FIRST_NAME = "NewFirstName";
    private static final String LAST_NAME = "LastName";
    private static final String NEW_LAST_NAME = "NewLastName";
    private static final String IBAN_1 = "ES6621000418401234567891";
    private static final String IBAN_2 = "ES1000492352082414205416";
    private static final String IBAN_3 = "ES6000491500051234567892";

    @Mock
    private User user;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    private User userUpdated;

    @BeforeEach
    public void setUp() throws Exception {
        userUpdated=  User.builder().firstName(NEW_FIRST_NAME).lastName(NEW_LAST_NAME)
                .ibans(aListWithTwoIbans(IBAN_1,IBAN_3)).build();
    }

    @Test
    public void shouldCreateAnUserWhenCreateUserIsCalled() {
        whenAnUserIsCreated();
        thenUserShouldBeSaved();
    }

    @Test
    public void shouldReturnAListOfUsersWhenGetAllUsersIsCalled() {
        givenAListOfUsers();
        List<User> users = whenGetAllUsersIsCalled();
        thenAllUsersAreReturned(users);
    }

    @Test
    public void shouldReturnAnEmptyListWhenGetAllUsersIsCalledAndThereAreNotUsers() {
        givenAnEmptyListOfUsers();
        List<User> users = whenGetAllUsersIsCalled();
        thenEmptyListIsReturned(users);
    }

    @Test
    public void shouldDeleteTheIbanWhenUserHasSeveralIbans() {
        givenAnUserFromIbanWithTwoIbansAssociated();
        whenDeleteIbanIsCalled();
        thenTheIbanIsDeleted();
    }

    @Test
    public void shouldUpdateAllFieldsOfAnUserWhenExistsAnUser() {
        givenAnUserFromIban();
        whenUpdateUserIsCalled();
        thenUserIsUpdated();
    }

    private void givenAListOfUsers() {
        when(userRepository.findAll()).thenReturn(aListOfUsers());
    }

    private void givenAnEmptyListOfUsers() {
        when(userRepository.findAll()).thenReturn(anEmptyListOfUsers());
    }

    private void givenAnUserFromIbanWithTwoIbansAssociated() {
        givenAnUserFromIban();
        when(user.getIbans()).thenReturn(aListWithTwoIbans(IBAN_1,IBAN_2));
    }

    private void givenAnUserFromIban() {
        when(userRepository.findByIbansIban(IBAN_1)).thenReturn(Optional.of(user));
    }

    private void whenAnUserIsCreated() {
        userService.createUser(user);
    }

    private List<User> whenGetAllUsersIsCalled() {
        return userService.getAllUsers();
    }

    private void  whenDeleteIbanIsCalled(){
        userService.deleteIban(IBAN_1);
    }

    private void whenUpdateUserIsCalled(){
        userService.updateUser(userUpdated, IBAN_1);
    }
    private void thenUserShouldBeSaved() {
        verify(userRepository).save(user);
    }

    private void thenAllUsersAreReturned(List<User> users) {
        assertEquals(1,users.size());
        assertEquals(users.get(0).getFirstName(), FIRST_NAME);
        assertEquals(users.get(0).getLastName(), LAST_NAME);
    }

    private void thenEmptyListIsReturned(List<User> users){
       assertEquals(0, users.size());
    }

    private void thenTheIbanIsDeleted(){
        assertEquals(1, user.getIbans().size());
        assertEquals(user.getIbans().get(0).getIban(), IBAN_2);
    }

    private void thenUserIsUpdated(){
        verify(userRepository).save(user);
    }

    private List<User> aListOfUsers() {
        List<User> users = new ArrayList<>();
        users.add(anUser());

        return users;
    }

    private List<User> anEmptyListOfUsers(){
        return Collections.emptyList();
    }

    private User anUser() {
        return User.builder().firstName(FIRST_NAME).lastName(LAST_NAME)
                .ibans(aListOfIbans()).build();
    }

    private List<Iban> aListOfIbans() {
        return Collections.singletonList(anIban(IBAN_1));
    }

    private List<Iban> aListWithTwoIbans(String iban1, String iban2) {
        return new java.util.ArrayList<>(Arrays.asList(anIban(iban1), anIban(iban2)));
    }

    private Iban anIban(String iban){
        return Iban.builder().iban(iban).build();
    }
}
