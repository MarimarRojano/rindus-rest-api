package com.rojanommar.rest.api.rindus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class RindusApplication {

	public static void main(String[] args) {
		SpringApplication.run(RindusApplication.class, args);
	}

}
