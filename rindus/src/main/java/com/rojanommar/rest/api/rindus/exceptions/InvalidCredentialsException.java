package com.rojanommar.rest.api.rindus.exceptions;

public class InvalidCredentialsException extends RuntimeException {

    private static final long serialVersionUID = 539409410402215593L;

    /**
     * Instantiates a entity exits exception.
     *
     * @param message
     */
    public InvalidCredentialsException(String message) {
        super("INVALID CREDENTIALS, please check the first name, last name and password");
    }

}
