package com.rojanommar.rest.api.rindus.service.impl;

import com.rojanommar.rest.api.rindus.dao.UserRepository;
import com.rojanommar.rest.api.rindus.exceptions.EntityConflictException;
import com.rojanommar.rest.api.rindus.model.User;
import com.rojanommar.rest.api.rindus.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public User createUser(User user) {
        logger.info("Creating new user {}", user);
        userRepository.findByFirstNameAndLastName(user.getFirstName(), user.getLastName()).ifPresent(user1 -> {
            throw new EntityConflictException();
        });
        user.getIbans().forEach(iban -> iban.setUser(user));
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User updateUser(User user, String iban) {
        logger.info("Updating user {}", user);
        Optional<User> userFromDB = userRepository.findByIbansIban(iban);
        userFromDB.ifPresent(u -> updateFields(u,user));
        return userFromDB.orElseGet(User::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        logger.info("Getting data user list");
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteIban(String iban) {
        logger.info("Deleting iban of user {}", iban);
        Optional<User> userFromDB = userRepository.findByIbansIban(iban);
        userFromDB.ifPresent(u -> removeIban(u, iban));
    }

    private void removeIban(User user, String iban) {
        if (!user.getIbans().isEmpty() && user.getIbans().size() > 1) {
            user.getIbans().removeIf(i -> i.getIban().equals(iban));
            userRepository.save(user);
        }
    }

    private void updateFields(User userDb, User user) {
        Optional.ofNullable(user.getFirstName()).ifPresent(v -> userDb.setFirstName((user.getFirstName())));
        Optional.ofNullable(user.getLastName()).ifPresent(v -> userDb.setLastName((user.getLastName())));
        userRepository.save(userDb);
    }
}
