package com.rojanommar.rest.api.rindus.dao;

import com.rojanommar.rest.api.rindus.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Find a user by iban.
     *
     * @param iban iban of user
     * @return user entity or empty value
     */
    Optional<User> findByIbansIban(String iban);

    /**
     * Find a user by first name and last name
     * @param firstName first name of user
     * @param lastName last name of user
     * @return user entity or empty value
     */
    Optional<User> findByFirstNameAndLastName(String firstName, String lastName);



}


