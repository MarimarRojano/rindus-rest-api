package com.rojanommar.rest.api.rindus.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.validator.routines.IBANValidator;

/**
 * The Class IbanValidator.
 */
public class IbanValidator implements ConstraintValidator<IbanValid, String> {

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.validation.ConstraintValidator#initialize(java.lang.annotation.
     * Annotation)
     */
    @Override
    public void initialize(IbanValid Iban) {

    }

    /*
     * (non-Javadoc)
     *
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object,
     * javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintContext) {
        return IBANValidator.getInstance().isValid(value);
    }
}
