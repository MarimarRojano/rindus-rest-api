package com.rojanommar.rest.api.rindus.exceptions;

public class EntityConflictException extends RuntimeException {

    private static final long serialVersionUID = 3628506809620944873L;

    /**
     * Instantiates a entity exits exception.
     *
     */
    public EntityConflictException() {
        super("Operation cannot be completed, the entity exists, please check data.");
    }

}
