package com.rojanommar.rest.api.rindus.controller;

import com.rojanommar.rest.api.rindus.exceptions.InvalidCredentialsException;
import com.rojanommar.rest.api.rindus.exceptions.UserDisabledException;
import com.rojanommar.rest.api.rindus.jwt.security.JwtTokenUtil;
import com.rojanommar.rest.api.rindus.model.User;
import com.rojanommar.rest.api.rindus.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * Controller for the User CRUD endpoints.
 */
@RestController
@Api(value="onlinestore")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailsService jwtInMemoryUserDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @ApiOperation(value = "Authenticate user", response =  User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully authenticate process"),
    }
    )
    @PostMapping("/users/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody User user)
            throws Exception {
        logger.info("Authenticate user {}.", user);
        authenticate(user.getUsername(), user.getPassword());
        UserDetails userDetails = jwtInMemoryUserDetailsService
                .loadUserByUsername(user.getUsername());
        return ResponseEntity.ok(jwtTokenUtil.generateToken(userDetails));
    }

    @ApiOperation(value = "Create user", response =  User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created user"),
    }
    )
    @PostMapping("/users")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        logger.info("Creating user {}", user);
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(user));
    }

    @ApiOperation(value = "Update user")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully updated user")
    }
    )
    @PutMapping("/users/{iban}")
    public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable String iban) {
        logger.info("Updating user {}.", user);
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.updateUser(user, iban));
    }

    @ApiOperation(value = "View a list of served orders")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    }
    )
    @GetMapping("/users")
    public ResponseEntity<?> getUsers() {
        logger.info("Getting users list");
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @ApiOperation(value = "Delete iban of user")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted user")
    }
    )
    @DeleteMapping("/users/{iban}")
    ResponseEntity<?> deleteIban(@PathVariable String iban) {
        logger.info("Deleting iban");
        userService.deleteIban(iban);
        return ResponseEntity.noContent().build();
    }

    private void authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new UserDisabledException(e.getMessage());
        } catch (BadCredentialsException e) {
            throw new InvalidCredentialsException(e.getMessage());
        }
    }
}
