package com.rojanommar.rest.api.rindus.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Iban validator interface.
 * @IbanValid
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IbanValidator.class)
public @interface IbanValid {

    String message() default "Iban not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
