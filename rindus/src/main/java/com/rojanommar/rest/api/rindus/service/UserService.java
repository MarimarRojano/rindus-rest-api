package com.rojanommar.rest.api.rindus.service;

import com.rojanommar.rest.api.rindus.model.User;
import java.util.List;

public interface UserService {

   User createUser(User user);
   User updateUser(User user, String iban);
   List<User> getAllUsers();
   void deleteIban(String iban);
}
