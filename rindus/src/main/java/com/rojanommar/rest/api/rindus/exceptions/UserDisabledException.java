package com.rojanommar.rest.api.rindus.exceptions;

public class UserDisabledException extends RuntimeException {

    private static final long serialVersionUID = 539409410402215593L;

    /**
     * Instantiates a entity exits exception.
     *
     * @param message
     */
    public UserDisabledException(String message) {
        super("USER DISABLED, please check the user");
    }

}
