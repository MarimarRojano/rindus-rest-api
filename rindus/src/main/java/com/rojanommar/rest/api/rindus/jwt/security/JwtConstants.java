package com.rojanommar.rest.api.rindus.jwt.security;

public class JwtConstants {

    /**
     * Constant that contain the urls without authorization.
     */
    public static final String[] URLS_WHITELIST = {
            "/swagger-ui.html",
            "/swagger-resources",
            "/swagger-resources/**",
            "/v2/api-docs",
            "/configuration/ui",
            "/configuration/security",
            "/webjars/**"
    };

    public static final long JWT_TOKEN_VALIDITY = 5*60*60;
}
