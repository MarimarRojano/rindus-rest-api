# Bank user API - Rindus #

Rest API (CRUD) with web page as GUI (Swagger) for managing users data and their bank account data.

### Deployment instructions ###

#### Prerequisites

You will need to install the following

- [Java 8](https://www.java.com/en/download/help/java8.html)
- [Maven 3](https://maven.apache.org/download.cgi)
- [Docker](https://www.docker.com/)

In order to run the application we will need to install Mysql too. We will use Docker for this.
#### Install Docker in your machine 
```
######## Crea container with mysql image
docker run -d -p 33060:3306 --name rindus-db -e MYSQL_ROOT_PASSWORD=secret mysql

######## Create database
docker exec rindus-db mysql -uroot -psecret -e "DROP SCHEMA IF EXISTS rindusdb; CREATE SCHEMA rindusdb;"
```

#### Install 

Step 1: Clone repository
```
git clone https://MarimarRojano@bitbucket.org/MarimarRojano/rindus-rest-api.git
```
Step 2: Run the next command
```
mvn spring-boot:run
```

Step 3: Go to
```
http://localhost:8082/swagger-ui.html
```

### Testing the API ###

#### Users for testing the application

This api uses JWT in order to do the login/logout. Due to this, there is a user in the application called "Java admin" (username) that will have permission for asking for one token. 
This user will be able to create users, get all users, update first name and last name of an user and delete an iban belongs to an user so implicitly this user has admin role and user role.

#### How to get a valid token and how to use it

Step 1: Go to Swagger page ( http://localhost:8082/swagger-ui.html)

Step 2: Click /users/authenticate method with the following payload
```
{
  "firstName": "java",
  "lastName": "admin",
  "password": "password"
}
```
Step 3 : Copy JWT token from response

Step 4 : Click in Authorize button at the top right of the page 

Step 5 : Write in value field "Bearer" space and token
```
 Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqYXZhIGFkbWluIiwiZXhwIjoxNjEzOTg5NDI2LCJpYXQiOjE2MTM5NzE0MjZ9.XWnK_u-SkTjpLjzchj3dNZUML2AgbHDceEyNJ9VfUYg
```

Step 6: Click Authorize button and close popup.

#### Possible issue

Depends on environment, you could have a issue if O.S. is Windows 7.
If in the starting of application appears "Connection failure" or some like that and you are using Windows pc you should update application.properties (rindus\src\main\resources\application.properties).
The spring.datasource.url value "0.0.0.0:3306" should be changed by HOST:IP of your virtual machine.
For example executing in Docker client we can see:

```
 $ docker-machine ls
```
```
NAME      ACTIVE   DRIVER       STATE     URL                         SWARM   DO
CKER      ERRORS
default   *        virtualbox   Running   tcp://192.168.99.100:2376           v1
9.03.12
```
So the new value will be 192.168.99.100:33060 (see below):

```
spring.datasource.url=jdbc:mysql://192.168.99.100:33060/rindusdb?useSSL=false
```
### Author ###

Maria del Mar Rojano